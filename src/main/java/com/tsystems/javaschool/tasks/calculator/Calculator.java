package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.Stack;

import static java.math.RoundingMode.HALF_UP;

public class Calculator {

    private static final String OPERATORS = "+*-/";

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            double result = calculate(postfix(statement));
            return new BigDecimal(result).setScale(4, HALF_UP).stripTrailingZeros().toPlainString();
        } catch (NullPointerException | EmptyStackException | NumberFormatException e) {
            return null;
        }
    }

    /**
     * Determinates precedence for further comparison.
     *
     * @param operator mathematical sign '+', '-', '*', '/' or parentheses operator '(', ')'
     * @return int representing the precedence power of operator.
     */
    private static int precedence(String operator) {
        switch (operator) {
            case "+":
            case "-":
                return 0;
            case "*":
            case "/":
                return 1;
            default:            // parenthesis operator "(" or ")" is expected
                return 2;
        }
    }

    /**
     * Represents the given arithmetic statement in Reverse Polish notation (RPN)
     *
     * @param infix arithmetic statement in infix notation
     * @return String value containing the result of transformation, with all operators separated by whitespaces
     */
    private static String postfix(String infix) {
        StringBuilder output = new StringBuilder();
        Deque<String> stack = new LinkedList<>();

        for (String token : infix.split("")) {
            if (OPERATORS.contains(token)) {
                output.append(' ');
                /*
                * Compares current operator with operators on the top of the stack based on precedence power.
                * The strongest one is appended to the output string while weaker ones go on the top of the stack.
                * */
                while (!stack.isEmpty()
                        && (precedence(token) <= precedence(stack.peek())) && precedence(stack.peek()) < 2) {
                    output.append(stack.pop()).append(' ');
                }
                stack.push(token);

            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                output.append(' ');
                while (!stack.peek().equals("(")) {
                    output.append(stack.pop());
                }
                stack.pop();
            } else {
                output.append(token);
            }
        }
        output.append(' ');
        while (!stack.isEmpty())
            output.append(stack.pop()).append(' ');
        return output.toString();
    }

    /**
     * Provides calculation on postfix representation of an arithmetical statement
     *
     * @param postfix arithmetic statement in Reverse Polish notation. All operators must be separated by whitespaces.
     * @return double as the result of calculation
     */
    private static double calculate(String postfix) {
        Stack<Double> numbers = new Stack<>();
        for (String token : postfix.split(" ")) {
            if (!OPERATORS.contains(token)) {
                numbers.push(Double.valueOf(token));
            } else {
                switch (token) {
                    case "+":
                        numbers.push(numbers.pop() + numbers.pop());
                        break;
                    case "-":
                        numbers.push(-numbers.pop() + numbers.pop());
                        break;
                    case "*":
                        numbers.push(numbers.pop() * numbers.pop());
                        break;
                    case "/":
                        double tmp = numbers.pop();
                        numbers.push(numbers.pop() / tmp);
                        break;
                }
            }
        }
        return numbers.pop();
    }
}