package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import java.util.*;

import static java.nio.charset.Charset.forName;
import static java.nio.file.Files.write;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import static java.util.Collections.frequency;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        Charset cs = forName("UTF-8");
        try {
            List<String> lines = readAllLines(get(sourceFile.getName()), cs);
            if (lines.isEmpty()) {
                return false;
            }
            Set<String> result = new LinkedHashSet<>();
            int n = 0;
            for (String line : lines ) {
                // Check for duplicates to not proceed frequency call on duplicates
                if (!result.contains(linePlusBrackets(line, n))) {
                    result.add(linePlusBrackets(line, n = frequency(lines, line)));
                }
            }
            if (targetFile.exists()) {
                result.removeAll(readAllLines(get(sourceFile.getName()), cs));
            }
            write(get(targetFile.getName()), result, cs);
        } catch (IOException | NullPointerException e) {
            throw new IllegalArgumentException();
        }
        return true;
    }

    /**
     * Concatenates @param line and given @param n surrounded by brackets.
     */
    private static String linePlusBrackets(String line, int n) {
        return line + "[" + n + "]" + "\n";
    }
}