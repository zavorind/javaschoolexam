package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try {
            int prevIndex = -y.size();  // for the sake of "y" not being null
            for (Object i : x) {
                int currIndex = y.indexOf(i);
                if (currIndex == -1 || currIndex < prevIndex) { // checks if "y" contains "i" and if order is correct
                    return false;
                }
                prevIndex = currIndex;
            }
        } catch (NullPointerException | ClassCastException e ) {
            throw new IllegalArgumentException();
        }
        return true;
    }
}